# OnlineStore TreasureHunt

Online Store: API-based program for online shopping 
Treasure Hunt: Command line based program for treasure search 

Online Store:
Cases of misreported or negative inventory quantity can occur because the process of reducing stock is carried out after the customer has made a payment. for example there are 5 stock items, then there are 10 people who see the stock is still available. then the customer checkout and payment at the same time. so that when the customer has made a payment, a new stock reduction is carried out so that it becomes -5 and there are 5 customers who have to cancel the order.

Therefore, to prevent this case from happening again, it could be done by reducing the stock before the payment process. for example there are 5 stock items, then there are 10 people who have successfully checkout and want to make a payment. because the stock will decrease when click payment (not waiting for the customer to have successfully made the payment), so there will be 5 people who fail to continue the payment process. then if out of 5 people who have made it to the payment process, but have failed the payment (maybe because time out or there is no media for payment) the stock will increase again.

List of available API endpoints:
1. GET: http://localhost/OnlineStore_TreasureHunt/Online%20Store/customer.php => To get all customer data
2. GET: http://localhost/OnlineStore_TreasureHunt/Online%20Store/product.php => To get all product data
3. GET: http://localhost/OnlineStore_TreasureHunt/Online%20Store/cart.php => To get all cart data
4. GET: http://localhost/OnlineStore_TreasureHunt/Online%20Store/order.php => To get all order data
5. POST: http://localhost/OnlineStore_TreasureHunt/Online%20Store/order.php => To insert order data and automatically reduce stock in product
6. POST: http://localhost/OnlineStore_TreasureHunt/Online%20Store/payment.php?order_id={order_id} => To update status and add stock if payment fails

===================================================================
Treasure Hunt:
1. Open cmd
2. Go to Treasure Hunt location
3. Run > php index.php