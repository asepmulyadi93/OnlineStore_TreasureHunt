<?php

include("data.php");

// Function to find a location
function findObracle($x, $y) {
	$obtacleData = getObtableData();

	foreach ($obtacleData as $key => $value) {
		if ($value[0] === $x && $value[1] === $y) {
			return true;
		}
	}

	return false;
}

function findClearPath($x, $y) {
	$clearPathData = getClearPathData();

	foreach ($clearPathData as $key => $value) {
		if ($value[0] === $x && $value[1] === $y) {
			return true;
		}
	}

	return false;
}


// Function to print the grid
function printGrid($startPosition, $treasureLocation=array(-1, -1)) {
	$status = false;

	for ($y=0; $y < 6; $y++) { 
		for ($x=0; $x < 8; $x++) { 
			if (findObracle($x, $y)) {
				echo '#';
			} else if (findClearPath($x, $y)) {
				if ($startPosition[0] === $x && $startPosition[1] === $y) {
					echo 'X';
					$status = true;
				} else if ($treasureLocation[0] === $x && $treasureLocation[1] === $y) {
					echo '$';
					$status = true;
				} else {
					echo '.';
				}
			} else {
				echo '+';
			}
		}

		echo PHP_EOL;
	}

	if ($status !== true) {
		echo "The starting position / location of the treasure is wrong, you have to place it in a clear path!\n";
	}
}

function printProbableTreasureLocations($startPosition) {
	echo "\nThe probable treasure locations:\n";

	$treasureLocationsData = getTreasureLocationsData();

	foreach ($treasureLocationsData as $key => $value) {
		printGrid($startPosition, $value);
		echo PHP_EOL;
	}
}

// Function to check user's position
function checkMove($startPosition) {
	$clearPathData = getClearPathData();

	foreach ($clearPathData as $key => $value) {
		if ($startPosition[0] === $value[0] && $startPosition[1] === $value[1]) {
			return true;
		}
	}

	return false;
}

function checkTreasure($startPosition) {
	$treasureLocationsData = getTreasureLocationsData();

	foreach ($treasureLocationsData as $key => $value) {
		if ($startPosition[0] === $value[0] && $startPosition[1] === $value[1]) {
			return true;
		}
	}

	return false;
}

// Start the program
$startPosition = array(1, 4);

printGrid($startPosition);
printProbableTreasureLocations($startPosition);

$status = "start";

echo "Information:\n";
echo "Up/North: A\n";
echo "Right/East: B\n";
echo "Down/South: C\n";
echo "Exit: exit\n";

do {
	echo "Command: ";
	$command = trim(fgets(STDIN));

	switch (strtolower($command)) {
  		case 'a':
  			$startPosition[1] -= 1;

  			if (!checkMove($startPosition)) {
  				echo "Wrong step, you can only move to clear paths.\n";
  				$startPosition[1] += 1;
  			}
	    	break;
	  	case 'b':
	  		$startPosition[0] += 1;

  			if (!checkMove($startPosition)) {
  				echo "Wrong step, you can only move to clear paths.\n";
  				$startPosition[0] -= 1;
  			}
			break;
		case 'c':
			$startPosition[1] += 1;

  			if (!checkMove($startPosition)) {
  				echo "Wrong step, you can only move to clear paths.\n";
  				$startPosition[1] -= 1;
  			}
			break;
		case "exit":
			break;
		default:
			echo "Input not recognized!\n";
	}

	printGrid($startPosition);

	if(checkTreasure($startPosition)){
		echo "Congratulations! You've got a treasure.\n";
		$status = "end";
	}
	
} while ($status !== "end" && strtolower($command) !== "exit");