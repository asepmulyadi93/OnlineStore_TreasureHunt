<?php

header("Content-Type:application/json");

include('db.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	// Get all customers
	try {
		$result = mysqli_query($con, "SELECT * FROM customers");

		if (mysqli_num_rows($result) > 0) {
			$row = array();

			while ($obj = $result -> fetch_object()) {
		    	$row[] = $obj;
		  	}

			response(200, $row, NULL);
		} else {
			response(200, $result, "No Record Found");
		}

		mysqli_close($con);
	} catch (Exception $ex) {
		response(500, NULL, $ex->getMessage());
	}
} else {
	response(404, NULL, "Function not found!");
}