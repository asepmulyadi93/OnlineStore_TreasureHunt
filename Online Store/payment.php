<?php

header("Content-Type:application/json");

include('db.php');
include('validation.php');

if ($_SERVER['REQUEST_METHOD'] === "POST") {
	// Insert to order and reduce product stock
	$validation = paymentValidation($_POST, $_GET, $con);
	
	if ($validation['status'] == 'success') {
		try {
			$status = $_POST['status'];
			$order_id = $_GET['order_id'];

			if (strtolower($status) === 'success') {
				// Update status order
				$sql = "UPDATE `orders` SET `order_status`='$status' WHERE id=$order_id";

				if ($con->query($sql) === TRUE) {
			  		response(200, array(), "Updated successfully");
				} else {
					response(500, array(), "Error: ".$sql."<br>".$con->error);
				}
			} else if (strtolower($status) === 'fail') {
				// Get order data because i need qty data of the order
				$order_result = mysqli_query($con, "SELECT * FROM orders WHERE id=$order_id");

				if (mysqli_num_rows($order_result) > 0) {
					$order_data = mysqli_fetch_object($order_result);
					$product_id = $order_data->product_id;

					// Get product data because i need stock data of the product
					$product_result = mysqli_query($con, "SELECT * FROM products WHERE id=$product_id");

					if (mysqli_num_rows($product_result) > 0) {
						// Increase stock
						$product_data = mysqli_fetch_object($product_result);
						$stock = $product_data->stock + $order_data->order_qty;

						$sql = "UPDATE `products` SET `stock`='$stock' WHERE id=$product_id";

						if ($con->query($sql) === TRUE) {
							// Update status order
							$sql = "UPDATE `orders` SET `order_status`='$status' WHERE id=$order_id";

							if ($con->query($sql) === TRUE) {
						  		response(200, array(), "Updated successfully");
							} else {
								response(500, array(), "Error: ".$sql."<br>".$con->error);
							}
						} else {
							response(500, array(), "Error: ".$sql."<br>".$con->error);
						}

					}
				}
			} else {
				response(404, array(), "Status can only be filled success / fail!");
			}
			
			mysqli_close($con);
		} catch (Exception $ex) {
			response(500, array(), $ex->getMessage());
		}
	} else {
		response(200, array(), $validation['message']);
	}
} else {
	response(404, NULL, "Function not found!");
}