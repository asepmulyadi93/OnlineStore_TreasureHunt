<?php

function orderValidation($data) {
	$result['status'] = 'success';
	$result['message'] = array();

	if (!isset($data['customer_id'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'customer_id is required.';
	}

	if (!isset($data['product_id'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'product_id is required.';
	}

	if (!isset($data['order_number'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'order_number is required.';
	}

	if (!isset($data['amount'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'amount is required.';
	}

	if (!isset($data['order_qty'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'order_qty is required.';
	}

	if (!isset($data['order_address'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'order_address is required.';
	}

	if (!isset($data['order_email'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'order_email is required.';
	}

	if (!isset($data['order_status'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'order_status is required.';
	}

	return $result;
}

function paymentValidation($data_post, $data_get, $con) {
	$result['status'] = 'success';
	$result['message'] = array();

	if (!isset($data_post['status'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'status is required.';
	}

	if (isset($data_get['order_id'])) {
		$order_id = $data_get['order_id'];

		$result_data = mysqli_query($con, "SELECT * FROM `orders` WHERE id=$order_id");

		if (mysqli_num_rows($result_data) == 0) {
			$result['status'] = 'failed';
			$result['message'][] = 'order_id not found.';
		}
	} else if (!isset($data_get['order_id'])) {
		$result['status'] = 'failed';
		$result['message'][] = 'order_id is required.';
	}

	return $result;
}