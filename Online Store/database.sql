create database my_store;

create table customers (
    id int not null auto_increment primary key,
    username varchar(50) not null,
    password varchar(50) not null,
    email varchar(50) not null,
    is_login boolean not null,
	last_login datetime not null,
	address text,
	phone varchar(15),
	update_date datetime not null,
	created_date datetime not null
);

create table products (
    id int not null auto_increment primary key,
    name varchar(50) not null,
    description text,
    price bigint not null,
    stock int not null,
	update_date datetime not null,
	created_date datetime not null
);

create table orders (
    id int not null auto_increment primary key,
    customer_id int not null,
    product_id int not null,
    order_number varchar(15) not null,
    amount bigint not null,
    order_qty int not null,
    order_address text not null,
    order_email varchar(50) not null,
    order_date datetime not null,
    order_status varchar(20) not null,
	update_date datetime not null,
	created_date datetime not null,
	constraint FK_CustomerOrder foreign key (customer_id) REFERENCES Customers(id),
	constraint FK_ProductOrder foreign key (product_id) REFERENCES Products(id)
);

create table carts (
    id int not null auto_increment primary key,
    customer_id int not null,
    product_id int not null,
	update_date datetime not null,
	created_date datetime not null,
	constraint FK_CustomerCart foreign key (customer_id) REFERENCES Customers(id),
	constraint FK_ProductCart foreign key (product_id) REFERENCES Products(id)
);

insert into `customers`(`username`, `password`, `email`, `is_login`, `last_login`, `address`, `phone`, `update_date`, `created_date`) 
    VALUES ('user01', 'user01', 'user01@gmail.com', false, now(), 'user01', '123456', now(), now());
insert into `customers`(`username`, `password`, `email`, `is_login`, `last_login`, `address`, `phone`, `update_date`, `created_date`) 
    VALUES ('user02', 'user02', 'user02@gmail.com', false, now(), 'user02', '123456', now(), now());
insert into `customers`(`username`, `password`, `email`, `is_login`, `last_login`, `address`, `phone`, `update_date`, `created_date`) 
    VALUES ('user03', 'user03', 'user03@gmail.com', false, now(), 'user03', '123456', now(), now());

INSERT INTO `products`(`name`, `description`, `price`, `stock`, `update_date`, `created_date`) 
    VALUES ('product 01', 'product 01', 10000, 10, now(), now());
INSERT INTO `products`(`name`, `description`, `price`, `stock`, `update_date`, `created_date`) 
    VALUES ('product 02', 'product 02', 20000, 20, now(), now());
INSERT INTO `products`(`name`, `description`, `price`, `stock`, `update_date`, `created_date`) 
    VALUES ('product 03', 'product 03', 30000, 30, now(), now());

INSERT INTO `carts`(`customer_id`, `product_id`, `update_date`, `created_date`) 
    VALUES (1, 1, now(), now());
INSERT INTO `carts`(`customer_id`, `product_id`, `update_date`, `created_date`) 
    VALUES (2, 2, now(), now());
INSERT INTO `carts`(`customer_id`, `product_id`, `update_date`, `created_date`) 
    VALUES (3, 3, now(), now());