<?php

function response($status_no, $data, $message) {
	$response['status_no'] = $status_no;
	$response['data'] = $data;
	$response['message'] = $message;

	$json_response = json_encode($response);

	echo $json_response;
}