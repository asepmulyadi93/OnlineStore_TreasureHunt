<?php

header("Content-Type:application/json");

include('db.php');
include('validation.php');

if ($_SERVER['REQUEST_METHOD'] === "GET") {
	// Get all orders
	try {
	 	$result = mysqli_query($con, "SELECT * FROM orders");

		if (mysqli_num_rows($result) > 0) {
			$row = array();

			while ($obj = $result -> fetch_object()) {
		    	$row[] = $obj;
		  	}

			response(200, $row, NULL);
		} else {
			response(200, $result, "No Record Found");
		}

		mysqli_close($con);
	} catch (Exception $ex) {
		response(500, NULL, $ex->getMessage());
	}
} else if ($_SERVER['REQUEST_METHOD'] === "POST") {
	// Insert to order and reduce product stock
	$validation = orderValidation($_POST);

	if ($validation['status'] == 'success') {
		try {
			$customer_id = $_POST['customer_id'];
			$product_id = $_POST['product_id'];
			$order_number = $_POST['order_number'];
			$amount = $_POST['amount'];
			$order_qty = $_POST['order_qty'];
			$order_address = $_POST['order_address'];
			$order_email = $_POST['order_email'];
			$order_status = $_POST['order_status'];

			// Reduce stock
			$product_result = mysqli_query($con, "SELECT * FROM products WHERE id=$product_id");

			if (mysqli_num_rows($product_result) > 0) {
				$row = mysqli_fetch_object($product_result);
				$stock = $row->stock;

				if ($stock > 0) {
					// If ready stock
					$stock = $stock - $order_qty;

					$sql = "UPDATE products SET stock=$stock WHERE id=$product_id";

					if ($con->query($sql) === FALSE) {
						response(500, NULL, "Error: ".$sql."<br>".$con->error);
					} else {
						// Insert to order
				     	$sql = "INSERT INTO orders(customer_id, product_id, order_number, amount, order_qty, order_address, order_email, order_date, order_status, update_date, created_date) VALUES ($customer_id, $product_id, '$order_number', $amount, $order_qty, '$order_address', '$order_email', now(), '$order_status', now(), now())";

				     	if ($con->query($sql) === TRUE) {
					  		response(200, $_POST, "New record created successfully");
						} else {
							response(500, NULL, "Error: ".$sql."<br>".$con->error);
						}
					}
				} else {
					response(200, NULL, "Out of stock!");
				}
			}

			mysqli_close($con);
		} catch (Exception $ex) {
			response(500, NULL, $ex->getMessage());
		}
	} else {
		response(200, NULL, $validation['message']);
	}
} else {
	response(404, NULL, "Function not found!");
}
