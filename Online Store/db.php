<?php

include('response.php');

try {
	$con = mysqli_connect("localhost", "root", "", "my_store");

	if (mysqli_connect_errno()) {
		response(500, NULL, "Failed to connect to MySQL: " . mysqli_connect_error());
	 	die();
	}
} catch (Exception $ex) {
	response(500, NULL, $ex->getMessage());
}